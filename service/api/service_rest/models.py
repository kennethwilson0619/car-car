from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200)


class Technician(models.Model):
    employee_id = models.CharField(max_length=200, unique=True)
    employee_name = models.CharField(max_length=200)

    def __str__(self):
        return self.employee_name


class AppointmentStatus(models.Model):
    name = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    appointment_time = models.DateTimeField()
    reason = models.TextField()

    technician = models.ForeignKey(
        Technician, related_name="service_appointments", on_delete=models.CASCADE)
    status = models.ForeignKey(
        AppointmentStatus, related_name="service_appointments", on_delete=models.PROTECT)
    is_vip = models.BooleanField(default=False)

    def finish(self):
        status = AppointmentStatus.objects.get(name="FINISHED")
        self.status = status
        self.save()

    def cancel(self):
        status = AppointmentStatus.objects.get(name="CANCELED")
        self.status = status
        self.save()

    def book(self):
        status = AppointmentStatus.objects.get(name="BOOKED")
        self.status = status
        self.save()

    def __str__(self):
        return self.vin + " " + str(self.appointment_time)

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = AppointmentStatus.objects.get(name="BOOKED")
        kwargs["is_vip"] = AutomobileVO.objects.filter(
            vin=kwargs["vin"]).count() > 0
        service_appointment = cls(**kwargs)
        service_appointment.save()
        return service_appointment
