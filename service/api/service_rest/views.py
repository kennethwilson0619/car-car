from .models import Technician, ServiceAppointment 
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import (
    TechnicianEncoder,
    ServiceAppointmentEncoder,
)

# Create your views here.


@require_http_methods(["GET", "POST"])
def api_service_appointments(request, vin=None):
    if request.method == "GET":
        if vin is not None:
            try:
                service_appointments = ServiceAppointment.objects.filter(
                    vin=vin)
                return JsonResponse(
                    {"service_appointments": service_appointments},
                    encoder=ServiceAppointmentEncoder,
                )
            except ServiceAppointment.DoesNotExist:
                return JsonResponse({"message": "Invalid automobile vin"}, status=404)
        else:
            service_appointments = ServiceAppointment.objects.all()
            return JsonResponse(
                {"service_appointments": service_appointments},
                encoder=ServiceAppointmentEncoder,
            )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(pk=content["technician"])
            content['technician'] = technician
            service_appointment = ServiceAppointment.create(**content)
            return JsonResponse(
                service_appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            return JsonResponse(
                {"message": "Could not create the service appointment",
                    "detail": str(e)}, status=400)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_service_appointment(request, pk):
    if request.method == "GET":
        try:
            service_appointment = ServiceAppointment.objects.get(pk=pk)
            return JsonResponse(
                service_appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Invalid service appointment id"}, status=404)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(pk=content["technician"])
                content['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"}, status=404)
        ServiceAppointment.objects.filter(pk=pk).update(**content)
        service_appointment = ServiceAppointment.objects.get(pk=pk)
        return JsonResponse(service_appointment, encoder=ServiceAppointmentEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def api_finish_service_appointment(request, pk):
    service_appointment = ServiceAppointment.objects.get(pk=pk)
    service_appointment.finish()
    return JsonResponse(
        service_appointment,
        encoder=ServiceAppointmentEncoder,
        safe=False
    )


@require_http_methods(["PUT"])
def api_cancel_service_appointment(request, pk):
    service_appointment = ServiceAppointment.objects.get(pk=pk)
    service_appointment.cancel()
    return JsonResponse(
        service_appointment,
        encoder=ServiceAppointmentEncoder,
        safe=False
    )


@require_http_methods(["PUT"])
def api_book_service_appointment(request, pk):
    service_appointment = ServiceAppointment.objects.get(pk=pk)
    service_appointment.book()
    return JsonResponse(
        service_appointment,
        encoder=ServiceAppointmentEncoder,
        safe=False
    )


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Exception as e:
            return JsonResponse(
                {"message": "Could not create the technician", "detail": str(e)}, status=400)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(pk=pk)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"}, status=404)
    elif request.method == "PUT":
        content = json.loads(request.body)
        Technician.objects.filter(pk=pk).update(**content)
        technician = Technician.objects.get(pk=pk)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
