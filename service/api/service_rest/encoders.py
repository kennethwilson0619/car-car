from common.json import ModelEncoder

from .models import AutomobileVO, Technician, ServiceAppointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "employee_name",
        "employee_id"
    ]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "appointment_time",
        "technician",
        "reason",
        "is_vip",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "status": o.status.name,
        }


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "color",
        "year",
        "vin",
    ]
