import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
from service_rest.models import AutomobileVO


def get_data():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for am in content['autos']:
        AutomobileVO.objects.update_or_create(
            import_href=am['href'],
            defaults={
                "color": am['color'],
                "year": am['year'],
                "vin": am['vin'],
            }
        )


def poll():
    while True:
        # print('Service poller polling for data')
        try:
            get_data()
            # Write your polling logic, here
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(3)


if __name__ == "__main__":
    poll()
