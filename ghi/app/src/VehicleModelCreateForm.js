import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const VehicleModelCreateForm = () => {
  const navigate = useNavigate();
  const [modelName, setModelName] = useState('');
  const [modelPictureUrl, setModelPictureUrl] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [manufacturers, setManufacturers] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  const loadManufacturers = async () => {
    const getManufacturersUrl = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(getManufacturersUrl);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const postVehicleModelUrl = 'http://localhost:8100/api/models/';
    const data = {
      name: modelName,
      picture_url: modelPictureUrl,
      manufacturer_id: manufacturer
    }
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    }
    const response = await fetch(postVehicleModelUrl, fetchConfig);
    if (response.ok) {
      navigate(`/inventories/vehicle-models`);
    }
    else {
      const error = await response.json();
      setErrorMessage(error.detail);
    }
    setModelName('');
    setModelPictureUrl('');
    setManufacturer('');
  }

  useEffect(() => {
    loadManufacturers();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-vehicle-model-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setModelName(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={modelName}
                placeholder="Name"
                required
                type="text"
                name="vehicle-model-name"
                id="vehicle-model-name"
                className="form-control"
              />
              <label htmlFor="vehicle-model-name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setModelPictureUrl(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={modelPictureUrl}
                placeholder="Picture URL"
                required
                type="url"
                name="picture-url"
                id="picture-url"
                className="form-control"
              />
              <label htmlFor="picture-url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                onChange={(e) => setManufacturer(e.target.value)}
                value={manufacturer}
                required
                name="manufacturer"
                id="manufacturer"
                className="form-select"
              >
                <option value="">Choose a manufacturer</option>
                {manufacturers.map((m) => (
                  <option key={m.id} value={m.id}>
                    {m.name}
                  </option>
                ))}
              </select>
            </div>
            <div
              className={`alert alert-danger mb-4 ms-2 ${
                errorMessage ? '' : 'd-none'
              }`}
              id="error-message"
            >
              {errorMessage}
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default VehicleModelCreateForm;
