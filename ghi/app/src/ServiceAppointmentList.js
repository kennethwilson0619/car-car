import React, { useState, useEffect } from 'react';

const ServiceAppointmentList = () => {
  const [shownServices, setShownServices] = useState([]);
  const [loadedServices, setLoadedServices] = useState([]);
  const [query, setQuery] = useState('');
  const loadAutomobiles = async () => {
    const getServicesUrl = 'http://localhost:8080/api/service-appointments/';
    const response = await fetch(getServicesUrl);
    if (response.ok) {
      const data = await response.json();
      setShownServices(data.service_appointments);
      setLoadedServices(data.service_appointments);
    }
  };
  useEffect(() => {
    loadAutomobiles();
    setQuery('');
  }, []);

  const handleAction = async (id, action) => {
    const cancelServiceUrl = `http://localhost:8080/api/service-appointments/${id}/${action}/`;
    const response = await fetch(cancelServiceUrl, {
      method: 'PUT',
    });
    if (response.ok) {
      loadAutomobiles();
      // const data = await response.json();
      // console.log('data', data);
    }
  };

  return (
    <>
      <div className="d-flex mb-5">
        <input
          className="form-control mr-sm-2"
          type="search"
          placeholder="Search By VIN #, Customer Name, or Technician Name"
          aria-label="Search"
          value={query}
          onChange={(e) => {
            setQuery(e.target.value);
            if (e.target.value === '') {
              setShownServices(loadedServices);
            }
            // setShownServices(
            //   loadedServices.filter(
            //     (s) =>
            //       s.vin.toLowerCase().includes(e.target.value.toLowerCase()) ||
            //       s.customer_name
            //         .toLowerCase()
            //         .includes(e.target.value.toLowerCase()) ||
            //       s.technician.employee_name
            //         .toLowerCase()
            //         .includes(e.target.value.toLowerCase()) ||
            //       s.status.toLowerCase().includes(e.target.value.toLowerCase())
            //   )
            // );
          }}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              setShownServices(
                loadedServices.filter(
                  (s) =>
                    s.vin
                      .toLowerCase()
                      .includes(e.target.value.toLowerCase()) ||
                    s.customer_name
                      .toLowerCase()
                      .includes(e.target.value.toLowerCase()) ||
                    s.technician.employee_name
                      .toLowerCase()
                      .includes(e.target.value.toLowerCase()) ||
                    s.status
                      .toLowerCase()
                      .includes(e.target.value.toLowerCase())
                )
              );
            }
          }}
        />
        <button
          className="btn btn-outline-success ms-3"
          onClick={() => {
            setShownServices(
              loadedServices.filter(
                (s) =>
                  s.vin.toLowerCase().includes(query.toLowerCase()) ||
                  s.customer_name.toLowerCase().includes(query.toLowerCase()) ||
                  s.technician.employee_name
                    .toLowerCase()
                    .includes(query.toLowerCase()) ||
                  s.status.toLowerCase().includes(query.toLowerCase())
              )
            );
          }}
        >
          search
        </button>
      </div>
      <h1>Service Appointments</h1>
      <table
        className={`table table-striped ${
          shownServices.length ? '' : 'd-none'
        }`}
      >
        {/* <table className= "table table-striped" > */}
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {shownServices
            .sort(
              (a, b) =>
                (({ BOOKED: 1, CANCELED: 2, FINISHED: 3 }[a.status] || 0) -
                ({ BOOKED: 1, CANCELED: 2, FINISHED: 3 }[b.status] || 0))
            )
            .map((s) => {
              const dt = new Date(s.appointment_time);
              const date = dt.toISOString().split('T')[0];
              // const time =
              //   dt.toLocaleTimeString().split(':')[0] +
              //   ' ' +
              //   dt.toLocaleTimeString().slice(-2);
              const time = dt.toLocaleTimeString();
              return (
                <tr
                  key={s.id}
                  className={`${s.is_vip ? 'fw-bold' : ''} ${
                    s.status == 'CANCELED' ? 'table-danger' : ''
                  } ${s.status == 'FINISHED' ? 'table-success' : ''}`}
                >
                  <td>{s.vin}</td>
                  <td>
                    {s.customer_name}
                    {s.is_vip && (
                      <div
                        className="badge badge-pill badge-dark text-warning me-3 ms-2"
                        style={{ backgroundColor: 'black' }}
                      >
                        VIP
                      </div>
                    )}
                  </td>
                  <td>{date}</td>
                  <td>{time}</td>
                  <td>{s.technician.employee_name}</td>
                  <td>{s.reason}</td>
                  {s.status == 'BOOKED' ? (
                    <td>
                      <button
                        onClick={() => handleAction(s.id, 'cancel')}
                        className="btn btn-danger me-1"
                      >
                        Cancel
                      </button>
                      <button
                        onClick={() => handleAction(s.id, 'finish')}
                        className="btn btn-success"
                      >
                        Finished
                      </button>
                    </td>
                  ) : s.status == 'CANCELED' ? (
                    <td>
                      <span className="badge badge-pill badge-secondary text-secondary me-2">
                        Canceled
                      </span>
                      <button
                        className="btn btn-primary"
                        onClick={() => handleAction(s.id, 'book')}
                      >
                        Rebook
                      </button>
                    </td>
                  ) : (
                    <td>
                      <span className="badge badge-pill badge-secondary text-secondary me-3">
                        Finished
                      </span>
                      <button
                        className="btn btn-primary"
                        onClick={() => handleAction(s.id, 'book')}
                      >
                        Rebook
                      </button>
                    </td>
                  )}
                </tr>
              );
            })}
        </tbody>
      </table>
      <div
        className={`alert alert-warning ${
          shownServices.length ? 'd-none' : ''
        } mt-4 `}
      >
        No service appointments found
      </div>
    </>
  );
};

export default ServiceAppointmentList;
