import React, {useState} from 'react'
import { useNavigate } from 'react-router-dom';

const CustomerCreateForm = () => {
  const [customerName, setCustomerName] = useState('');
  const [customerAddress, setCustomerAddress] = useState('');
  const [customerPhone, setCustomerPhone] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = { 
      name: customerName,
      address: customerAddress,
      phone: customerPhone
     };
    const postCustomer = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    };
    const response = await fetch(postCustomer, fetchConfig);
    if (response.ok) {
      navigate(`/sales/customers/new`);
    } else {
      const error = await response.json();
      setErrorMessage(error.detail);
    }
    setCustomerName('');
    setCustomerAddress('');
    setCustomerPhone('');
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setCustomerName(e.target.value);
                }}
                value={customerName}
                placeholder="Name"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="customer">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setCustomerAddress(e.target.value);
                }}
                value={customerAddress}
                placeholder="Address"
                required
                type="text"
                name="address"
                id="address"
                className="form-control"
              />
              <label htmlFor="address">Address</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setCustomerPhone(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={customerPhone}
                placeholder="Phone Number"
                required
                type="text"
                name="phone"
                id="phone"
                className="form-control"
              />
              <label htmlFor="phone">Phone Number</label>
            </div>
            <div
              className={`alert alert-danger mb-4 ms-2 ${
                errorMessage ? '' : 'd-none'
              }`}
              id="error-message"
            >
              {errorMessage}
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CustomerCreateForm