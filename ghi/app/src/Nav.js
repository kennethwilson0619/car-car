import { Link, NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success mb-4">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventories
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link
                    className="dropdown-item"
                    to="/inventories/manufacturers"
                  >
                    Manufactures
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item"
                    to="/inventories/manufacturers/new"
                  >
                    Add a Manufacturer
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item"
                    to="/inventories/vehicle-models"
                  >
                    Vehicle Models
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item"
                    to="/inventories/vehicle-models/new"
                  >
                    Add a Vehicle Model
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/inventories/automobiles">
                    Automobiles
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item"
                    to="/inventories/automobiles/new"
                  >
                    Add a Automobile
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="/sales">
                    Sales
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/sales/salespersons/new">
                    Add a Sales Person
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/sales/customers/new">
                    Add a Customer
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/sales/new">
                    Add a Sales Record
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Services
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link
                    className="dropdown-item"
                    to="/services/technicians/new"
                  >
                    Add a Technician
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/services/new">
                    Add a Service Appointment
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/services">
                    Service Appointments
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
