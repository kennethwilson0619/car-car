import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const ServiceAppointmentCreateForm = () => {
  const navigate = useNavigate();
  const [vin, setVin] = useState('');
  const [customerName, setCustomerName] = useState('');
  const [appointmentTime, setAppointmentTime] = useState('');
  const [appointmentDate, setAppointmentDate] = useState('');
  const [technician, setTechnician] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [reason, setReason] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  const loadTechnicians = async () => {
    const getTechnicianUrl = 'http://localhost:8080/api/technicians/';
    const response = await fetch(getTechnicianUrl);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const postServiceUrl = 'http://localhost:8080/api/service-appointments/';
    const parsed_dt = parseDateTime(appointmentDate, appointmentTime);
    const data = {
      vin,
      technician,
      reason,
      appointment_time: parsed_dt,
      customer_name: customerName,
    };
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    };
    const response = await fetch(postServiceUrl, fetchConfig);
    if (response.ok) {
      // const newService = await response.json();
      // console.log(newService);
      navigate(`/services`);
    } else {
      const error = await response.json();
      setErrorMessage(error.message);
    }
    setVin('');
    setCustomerName('');
    setAppointmentDate('');
    setAppointmentTime('');
    setTechnician('');
    setReason('');
  };

  useEffect(() => {
    loadTechnicians();
  }, []);

  const parseDateTime = (date, time) => {
    if (!date || !time) {
      return null;
    }
    const timeString = `${date}T${time}`;
    const newDate = new Date(timeString);
    try {
      let res = newDate.toISOString();
      return res;
    } catch (e) {
      return '';
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-service-appointment-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setVin(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={vin}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="service-appointment">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setCustomerName(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={customerName}
                placeholder="Customer Name"
                required
                type="text"
                name="customer-name"
                id="customer-name"
                className="form-control"
              />
              <label htmlFor="customer-name">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setAppointmentDate(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={appointmentDate}
                placeholder="Appointment Time"
                required
                type="date"
                name="appointment-time"
                id="appointment-time"
                className="form-control"
              />
              <label htmlFor="appointment-time">Appointment Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setAppointmentTime(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={appointmentTime}
                placeholder="Appointment Time"
                required
                type="time"
                name="appointment-time"
                id="appointment-time"
                className="form-control"
              />
              <label htmlFor="appointment-time">Appointment Time</label>
            </div>
            <div className="mb-3">
              <select
                onChange={(e) => setTechnician(e.target.value)}
                value={technician}
                required
                name="technician"
                id="technician"
                className="form-select"
              >
                <option value="">Choose a technician</option>
                {technicians.map((t) => (
                  <option key={t.id} value={t.id}>
                    {t.employee_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setReason(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={reason}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <div
              className={`alert alert-danger mb-4 ms-2 ${
                errorMessage ? '' : 'd-none'
              }`}
              id="error-message"
            >
              {errorMessage}
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ServiceAppointmentCreateForm;
