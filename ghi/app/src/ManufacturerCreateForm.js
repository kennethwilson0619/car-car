import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const ManufacturerCreateForm = () => {
  const [manufacturer, setManufacturer] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = { name: manufacturer };
    const postManufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    };
    const response = await fetch(postManufacturerUrl, fetchConfig);
    if (response.ok) {
      // const newManufacturer = await response.json();
      // console.log(newManufacturer);
      navigate(`/inventories/manufacturers`);
    } else {
      const error = await response.json();
      setErrorMessage(error.detail);
    }
    setManufacturer('');
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setManufacturer(e.target.value);
                }}
                onFocus={() => {
                  setErrorMessage('');
                }}
                value={manufacturer}
                placeholder="Name"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Name</label>
            </div>
            <div
              className={`alert alert-danger mb-4 ms-2 ${
                errorMessage ? '' : 'd-none'
              }`}
              id="error-message"
            >
              {errorMessage}
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ManufacturerCreateForm;
