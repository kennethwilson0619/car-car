# CarCar

Team:

- Kenneth Wilson - Sales Microservice
- Jamie Liu - Service Microservice

## Design

https://excalidraw.com/#json=SEmc_8_M7ye2yHvEibrcZ,KELbJtarQPDp_ycKxuvORg
## Instruction to run the project

1. In your terminal cd into the project directory, and run following commands:
   ```
   docker volume create project-beta
   docker-compose build
   docker-compose up
   (initial data is seeded with migration)
   ```
2. visit `localhost:3000` on your browser

## Service microservice

- url and port of the service microservice: `localhost:8080`

- apis endpoints / crud routes

  1. endpoint: `/api/service-appointments/`
     - request method: `GET`: returns a list of all service appointments. Returning services' id, vin, customer_name, appointment_time, technician, reason, is_vip info.
     - request method: `POST`: create a service appointment object in our db. Returning the newly created appointment if successful, otherwise returns an error message showing why action failed.
  2. endpoint: `/api/vin/<vin_number>/service-appointments/`:
     - request method: `GET`: returns a list of service appointments under a certain vin.
  3. endpoint: `/api/service-appointments/<id>/`
     - request method: `GET`: returns the appointment detail of the service appointment with the appointment id provided.
     - request method: `PUT`: find the appointment with the provided id, update it with the payload, then returns the updated appointment info.
     - request method: `DELETE`: destroy an appointment record, returns a boolean to indicate if the delete is successful.
  4. endpoint: `/api/service-appointments/<id>/finish/`
     - request method: `PUT`: change the status of a service appointment to be 'FINISHED'
  5. endpoint: `/api/service-appointments/<id>/cancel/`
     - request method: `PUT`: change the status of a service appointment to be 'CANCELED'
  6. endpoint: `/api/service-appointments/<id>/book/`
     - request method: `PUT`: change the status of a service appointment to be 'BOOKED'
  7. endpoint: `/api/technicians/`
     - request method: `GET`: returns a list of all the technicians in our db. Returning their id, employee_id, and employee_name
     - request method: `POST`: create a new technician using the payload, returns the newly created record info.
  8. endpoint: `/api/technicians/<id>/`
     - request method: `GET`: returns the detail of the technician with the id provided.
     - request method: `PUT`: find the technician with the provided id, update it with the payload, then returns the updated technician info.
     - request method: `DELETE`: delete a technician from the system, returns a boolean indicating the delete status.

- models

  1. Technician:

     - fields:
       1. employee_id: string
       2. employee_name: string

  2. AppointmentStatus (value object):

     - fields:
       1. name: ENUM of 'BOOKED', 'CANCELED', and 'FINISHED'

  3. ServiceAppointment:

     - fields:
       1. vin: string
       2. customer_name: string
       3. appointment_time: datetime
       4. reason: string
       5. technician: Technician
       6. status: Status
       7. is_vip: boolean

  4. AutomobileVO (value object, data polled from Inventory app's api/automobiles endpoint)
     - fields:
       1. color: string
       2. year: integer
       3. vin: string
       4. import_href: string, is the equivalent of href of Inventory's Automobile data.

## Sales microservice

* url and port of the service microservice: localhost:8090


* apis endpoints / crud routes

    1. endpoint: /api/sales-persons/

        * request method: GET: returns a list of all sales persons. Returning sales_person id, name and employee_number.
        * request method: POST: create a sales person object in our db. Returning the newly created sales person if successful, otherwise returns an error message showing why action failed.


    2. endpoint: /api/sales-persons/<int:id>/:

        * request method: GET: returns details of a specific sales person based on their id. Returns id, name and employee_number.
        * request method: PUT: find the sales person with the provided id, update it with their name, then returns the updated sales person.
        * request method: DELETE: destroy a sales person record, returns a boolean to indicate if the delete is successful.


    3. endpoint: /api/sales/

        * request method: GET: returns a list of all sales recorded. Returning the id, sales_person, automobile, price and customer.
        * request method: POST: Create sale object in our db. Returning a newly created sale, if successful will book the sale and remove the automobile from inventory.


    4. endpoint: /api/sales/<int:id>/

        * request method: GET: Will return list of details pertaining to a specific sales record. Returning the sales_person, automobile, price and customer.
        * request method: DELETE: Will destroy a sales record, returns a boolean to indicate if the delete was successful

    5. endpoint: /api/customers/

        * request method: GET: returns a list of all the customers in our db. Returning their id, name, address and phone.
        * request method: POST: create a new customer using the name, address and phone.


    6. endpoint: /api/customers/<int:id>/

        * request method: GET: returns the detail of the customer with the id provided.
        * request method: PUT: find the customer with the provided id, update it with the name or phone or address, then returns the updated customer info.
        * request method: DELETE: delete a customer from the system, returns a boolean indicating the delete status.





* models


    1. AutomobileVO (value object, data polled from Inventory app's api/automobiles endpoint):

        * fields:

            * import_href: string
            * vin: string
            * year: int
            *   color: string




        2. SalesPerson:

            * fields:

                * name: string
                * employee_number: int





        3. Customer:

            * fields:

                * name: string
                * address: string
                * phone: int

        4. Sale:

            * fields:

                * sales_person: string
                * automobile: string
                * customer: string
                * price: int
